package com.Assignment;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Driver;
import java.time.Duration;

import org.apache.poi.sl.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Demo_Application_Test {
	
	
    WebDriver driver;
   static Select select;
   static JavascriptExecutor js;
   Object[][] data=null;
   File f;
   @DataProvider(name="login")
   private Object[][] dataDriven() throws BiffException, IOException {
         FileInputStream fis= new FileInputStream(f);
         f=new File("src/test/resources/Properties/Data.xls");

         Workbook book=Workbook.getWorkbook(fis);
         Sheet sh=book.getSheet("Sheet1");
         int rowCount= sh.getRows();
         int columnsCount=sh.getColumns();
         Object fd[i-1][j]=new Object[rowCount-1][columnsCount];
   }
   
   @BeforeClass
   private WebDriver launchBrowser() {
if (driver==null) {
           
           
           driver= new ChromeDriver();
           driver.get("https://admin-demo.nopcommerce.com/login");
           driver.manage().window().maximize();
           driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
       }
       return driver;
   }
   
   @Test(priority = 1)
   private void login() throws IOException {
       driver.findElement(By.xpath("//button[text()='Log in']")).click();
       String text = driver.findElement(By.xpath("//a[text()='John Smith']")).getText();
       Assert.assertEquals(text, "John Smith");
       WebElement categories = driver.findElement(By.xpath("//p[text()=' Categories']"));
       js= (JavascriptExecutor)driver;
       js.executeScript("arguments[0].click()",categories );
       
   }
   @Test(priority = 2)
   private void categories() {
       driver.findElement(By.xpath("//a[@href='/Admin/Category/Create']")).click();
       driver.findElement(By.id("Name")).sendKeys("Dell");
       WebElement selectEle = driver.findElement(By.id("ParentCategoryId"));
       select = new Select(selectEle);
       select.selectByVisibleText("Computers >> Build your own computer");
       driver.findElement(By.name("save")).click();
       driver.findElement(By.id("SearchCategoryName")).sendKeys("Lenovo");
       driver.findElement(By.id("search-categories")).click();
       String text = driver.findElement(By.xpath("//td[contains(text(),'Lenovo')]")).getText();
       Assert.assertTrue(text.contains("Lenovo"));
   }
       
       @Test(priority =3)
       private void products() {
          
            driver.findElement(By.partialLinkText("Products")).click();
             driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
             WebElement testDropDown = driver.findElement(By.id("SearchCategoryId"));  
             Select dropdown = new Select(testDropDown); 
             dropdown.selectByIndex(2);
             driver.findElement(By.id("search-products")).click();
           
       }
       @Test(priority = 4)
       private void manufacturers() {
                        driver.findElement(By.xpath("//p[text()=' Manufacturers']")).click();
             driver.findElement(By.xpath("//a[@href='/Admin/Manufacturer/Create']")).click();
             driver.findElement(By.id("Name")).sendKeys("Lenovo");
             driver.findElement(By.name("save")).click();
             String ActualProduct="Lenovo";
             String expectedProduct=driver.findElement(By.xpath("//td[contains(text(),'Lenovo')]")).getText();
             Assert.assertTrue(expectedProduct.contains(ActualProduct));
       }
       @AfterClass
       private void logout() {
           driver.findElement(By.xpath("//a[text()='Logout']")).click();
           driver.close();
       }

}
